using Microsoft.AspNetCore.Identity;

namespace DymanicRolesSample.Models
{
    public class ApplicationRole : IdentityRole
    {
        public string Access { get; set; }
        
    }
}