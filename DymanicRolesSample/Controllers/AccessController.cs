using System.ComponentModel;
using Microsoft.AspNetCore.Mvc;

namespace DymanicRolesSample.Controllers
{
    [DisplayName("Access Management")]
    public class AccessController : Controller
    {
        // GET
        [DisplayName("Access List")]
        public IActionResult Index()
        {
            return
            View();
        }
    }
}